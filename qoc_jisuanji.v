From Qoc Require Import Jisuanji .
  
(**

(1) 유한 이진 값은 다음과 같습니다. 거짓.

(2) 무한 수 : 0; 1 ; 2 = 다음 1; 3 = 다음 2; 4 = 다음 3; ....
그러나 숫자는 추가 데이터를 전송할 수 없습니다. 나는 데이터를위한 새로운 컨테이너 구조를 정의 할 것이다.이 구조는 "sequences"또는 "lists"로 명명된다.

(3) 시퀀스는 데이터를 전달할 수 있습니다. 예를 들어, 시퀀스의이 데이터는 동물 일 수 있습니다 : []; [cat]; [개, 고양이]; [물고기, 개, 고양이] ...
또 다른 예로, 시퀀스의 데이터는 색상 일 수 있습니다 : []; [레드]; [ 파랑 빨강 ] ; [골드, 블루, 레드] ...

(4) 서열이 많은 형태의 데이터를 포함 할 수 있다는 생각은 다형성 (polymorphism)으로 명명된다.

(5) 표기법은이 다형성을 숨길 수 있습니다. 그래서 그 대신에 (JoinOne animals fish (JoinOne animals dog (JoinOne animals cat (Empty animals))))); 그런 다음 [물고기, 개, 고양이]와 같은 것을 쓰고 "동물"인 데이터 유형을 숨 겼습니다.

(6.) 마지막으로, 모든 다형성 함수에 대해 한 번만 다형성 함수 "bottom"을 프로그래밍 할 수도 있습니다. 나는 하나만 프로그래밍하지만, (바닥 [물고기, 개, 고양이]) = 고양이; 나는 또한 밑바닥 [금, 청, 적] = 적색과 같은 것을 쓸 수있다.

(7. 물고기), (나머지 [물고기, 개, 고양이]) = [물고기], [물고기], [물고기], [물고기], [물고기], [고양이])와 같은 다형성 함수를  개, 고양이].

(8.) 다형성 객체 외에도 매개 변수 값 / 화살표를 유추 / 암시 적 / 표기 할 수 있습니다. 이것은 다음 라이브 녹음에서 제공됩니다.

*) 

꾸러미 COK_PolymorphismInductiveImplicitNotations .

유도성 listPoly (data : 종류) : 종류 := 
  Empty : listPoly data
| JoinOne : 능 (d : data) (l : listPoly data), listPoly data.

정의 rest_of_listPoly : 능 (data : 종류) (l : listPoly data), listPoly data .
증명 .
  많은소개 data l . 탈구축 l 같이 [ | dat l' ] .
  - 정확한 (Empty data) .
  - 정확한 l' .
정의된 .

유도성 binary : 종류 :=
  true : binary
| false : binary .

계산 (rest_of_listPoly binary (JoinOne binary false
                                          (JoinOne binary false
                                                   (JoinOne binary true
                                                            (Empty binary))))) .

유도성 infiniteNumbers : 종류 :=
  Zero : infiniteNumbers
| NextOne : infiniteNumbers -> infiniteNumbers .

계산 (rest_of_listPoly infiniteNumbers (JoinOne infiniteNumbers (NextOne (NextOne Zero))
                                          (JoinOne infiniteNumbers Zero
                                                   (JoinOne infiniteNumbers (NextOne Zero)
                                                            (Empty infiniteNumbers))))) .


계산 (JoinOne binary true (Empty binary)).
계산 (JoinOne _ true (Empty _)).
계산 (JoinOne _ (NextOne (NextOne Zero)) (Empty _)).
(**MEMO: polymorphism-objects can be inferred-implicit , via [표기법] command *)
표기법 "d :: l" := (JoinOne _ d l) .
표기법 "!00!" := (Empty _) .
계산 ( true :: !00! ).
계산 ( (NextOne (NextOne Zero)) :: !00! ).


계산 (rest_of_listPoly _ ( false ::  false ::  true :: !00! ) ) .
계산 (rest_of_listPoly _ ( (NextOne (NextOne Zero)) :: Zero :: (NextOne Zero) :: !00! ) ) .
표기법 "'rest'" := ( rest_of_listPoly _ ) .
계산 (rest ( false ::  false ::  true :: !00! ) ) .
(**MEMO: polymorphism-objects can be inferred-implicit , via [입력] command *)
입력 rest_of_listPoly [data] l .
계산 (rest_of_listPoly ( false ::  false ::  true :: !00! ) ) .


부분 section_polymorphism .

  변수 data : 종류 .

  (** the precise form/type of the output 
      ( precisely [unit] or [data] ? ) depends on the input *)
  유도성 optionPoly : 종류 := 
    Input_Invalid : (* unit -> *) optionPoly
  | Input_Valid : data -> optionPoly .

끝 section_polymorphism .

인쇄 optionPoly .  인쇄 unit .

설정 절대적인 입력 .

(** in some sense , the precise form/type of the output 
    ( precisely [unit] or [data] ? ) depends on the (parameter of the) input
    ( whether [l] is invalid or valid ? ) *)
정의 top_of_listPoly : 능 (data : 종류) (l : listPoly data), optionPoly data.
증명 .
  많은소개 data l . 경우 l .
  - 정확한 (Input_Invalid data). 
  - 많은소개 dat l' .
    대다 (Input_Valid data) .
    정확한 dat .
정의된 .

계산 (top_of_listPoly ( false ::  false ::  true :: !00! ) ) .

고정점 bottom_of_listPoly (data : 종류) (l : listPoly data) {구조 l} : optionPoly data .
증명 .
  탈구축 l 같이 [ | dat l' ] .
  - 정확한 (Input_Invalid data) .
  - 경우 (bottom_of_listPoly data l') .
    + 정확한 (Input_Valid data dat).
    + 지우다 l' . 많은소개 bottom_of_listPoly_data_l' .
      대다 (Input_Valid data).
      정확한 bottom_of_listPoly_data_l' .
정의된 .

계산 (bottom_of_listPoly ( false ::  false ::  true :: !00! )) .

끝 COK_PolymorphismInductiveImplicitNotations .



(** ------------------------------------------------------------------------- *)



(**

(1.) The finite binary values are : true ; false .

(2.) The infinite numbers are : 0 ; 1 ; 2 = next 1 ; 3 = next 2 ; 4 = next 3 ; ....
But the numbers cannot carry additional data .  I shall define some new container structure for data , which  is named "sequences" or "lists" .

(3.) The sequences can carry data . For example , this data of the sequence may be animals : [ ] ; [ cat ] ; [ dog , cat ] ; [ fish , dog , cat ] ...
Another example , this data of the sequence may be colors : [ ] ; [ red ] ; [ blue , red ] ; [ gold , blue , red  ] ...

(4.) This idea that the sequences may contain data of many forms is named as polymorphism .

(5.) The notations allow to hide this polymorphisms . So that instead that I write something such as (JoinOne animals fish (JoinOne animals dog (JoinOne animals cat (Empty animals)))) ; then I write something such as [ fish , dog , cat ] , and I have hidden the data type which is "animals" .

(6.) Finally , I may also program some polymorphism function "bottom" only once for all the different data types . I program only one , but I can write something such as ( bottom [ fish , dog , cat ] ) = cat ; and I can also write something such as ( bottom [ gold , blue , red  ] ) = red .

(7.) I will also show how to write the polymorphism functions "top" and "rest" , such that ( top [ fish , dog , cat ] ) = fish , and ( rest [ fish , dog , cat ] ) = [ dog , cat ] .

(8.) In addition to polymorphism-objects , parameters-values/arrows can also be inferred/implicit/notational . This will be presented in the next live transcription .
*)

Module PolymorphismInductiveImplicitNotations .

Inductive listPoly (data : Type) : Type := 
  Empty : listPoly data
| JoinOne : forall (d : data) (l : listPoly data), listPoly data.

Definition rest_of_listPoly : forall (data : Type) (l : listPoly data), listPoly data .
Proof .
  intros data l . destruct l as [ | dat l' ] .
  - exact (Empty data) .
  - exact l' .
Defined .

Inductive binary : Type :=
  true : binary
| false : binary .

Compute (rest_of_listPoly binary (JoinOne binary false
                                          (JoinOne binary false
                                                   (JoinOne binary true
                                                            (Empty binary))))) .

Inductive infiniteNumbers : Type :=
  Zero : infiniteNumbers
| NextOne : infiniteNumbers -> infiniteNumbers .

Compute (rest_of_listPoly infiniteNumbers (JoinOne infiniteNumbers (NextOne (NextOne Zero))
                                          (JoinOne infiniteNumbers Zero
                                                   (JoinOne infiniteNumbers (NextOne Zero)
                                                            (Empty infiniteNumbers))))) .


Compute (JoinOne binary true (Empty binary)).
Compute (JoinOne _ true (Empty _)).
Compute (JoinOne _ (NextOne (NextOne Zero)) (Empty _)).
(**MEMO: polymorphism-objects can be inferred-implicit , via [Notation] command *)
Notation "d :: l" := (JoinOne _ d l) .
Notation "!00!" := (Empty _) .
Compute ( true :: !00! ).
Compute ( (NextOne (NextOne Zero)) :: !00! ).


Compute (rest_of_listPoly _ ( false ::  false ::  true :: !00! ) ) .
Compute (rest_of_listPoly _ ( (NextOne (NextOne Zero)) :: Zero :: (NextOne Zero) :: !00! ) ) .
Notation "'rest'" := ( rest_of_listPoly _ ) .
Compute (rest ( false ::  false ::  true :: !00! ) ) .
(**MEMO: polymorphism-objects can be inferred-implicit , via [Arguments] command *)
Arguments rest_of_listPoly [data] l .
Compute (rest_of_listPoly ( false ::  false ::  true :: !00! ) ) .


Section section_polymorphism .

  Variable data : Type .

  (** the precise form/type of the output 
      ( precisely [unit] or [data] ? ) depends on the input *)
  Inductive optionPoly : Type := 
    Input_Invalid : (* unit -> *) optionPoly
  | Input_Valid : data -> optionPoly .

End section_polymorphism .

Print optionPoly .  Print unit .

Set Implicit Arguments .

(** in some sense , the precise form/type of the output 
    ( precisely [unit] or [data] ? ) depends on the (parameter of the) input
    ( whether [l] is invalid or valid ? ) *)
Definition top_of_listPoly : forall (data : Type) (l : listPoly data), optionPoly data.
Proof .
  intros data l . case l .
  - exact (Input_Invalid data). 
  - intros dat l' .
    apply (Input_Valid data) .
    exact dat .
Defined .

Compute (top_of_listPoly ( false ::  false ::  true :: !00! ) ) .

Fixpoint bottom_of_listPoly (data : Type) (l : listPoly data) {struct l} : optionPoly data .
Proof .
  destruct l as [ | dat l' ] .
  - exact (Input_Invalid data) .
  - case (bottom_of_listPoly data l') .
    + exact (Input_Valid data dat).
    + clear l' . intros bottom_of_listPoly_data_l' .
      apply (Input_Valid data).
      exact bottom_of_listPoly_data_l' .
Defined .

Compute (bottom_of_listPoly ( false ::  false ::  true :: !00! )) .

End PolymorphismInductiveImplicitNotations .

